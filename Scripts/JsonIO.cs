﻿using UnityEngine;
using System.IO;
using System.Collections;

/************************************************************************************************
 Copyright Emilio Gerardo Estrada Lucero (c) <2016> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
************************************************************************************************/

public static class JsonIO {

	public static bool DoesPathExist(string filename, LoadingPath pathType = LoadingPath.PersistentDataPath,
		FileExtension extensionType = FileExtension.json){
		return (File.Exists(PathReturner.ReturnLoadPath(pathType) + "/" 
			+ filename + PathReturner.ReturnFileExtension(extensionType)));
		
	}
	
	public static string FileToString(string filename, LoadingPath pathType = LoadingPath.PersistentDataPath,
		FileExtension extensionType = FileExtension.json) {
		string fullPath = PathReturner.ReturnLoadPath(pathType) + "/" 
			+ filename + PathReturner.ReturnFileExtension(extensionType);
		if(File.Exists(fullPath)) {
			var jsonToRead = File.ReadAllText(Application.persistentDataPath + "/" + filename + ".json");
			return jsonToRead;
			
		} else {
			Debug.LogError("JsonIO, Path does not exist: " + fullPath);
			return "";
		}
	}

	public static T FileToClass <T> (string filename, LoadingPath pathType = LoadingPath.PersistentDataPath,
		FileExtension extensionType = FileExtension.json) {
		string jsonString = FileToString(filename, pathType, extensionType);
		return JsonUtility.FromJson<T>(jsonString);
	}

	public static void FileToScriptableObject(object obj, string filename, LoadingPath pathType = LoadingPath.PersistentDataPath,
		FileExtension extensionType = FileExtension.json) {
		FileToMonobehaviour(obj, filename, pathType, extensionType);
	}


	public static void FileToMonobehaviour (object obj, string filename, LoadingPath pathType = LoadingPath.PersistentDataPath,
		FileExtension extensionType = FileExtension.json) {
		string jsonString = FileToString(filename, pathType, extensionType);
		JsonUtility.FromJsonOverwrite(jsonString, obj);
	}
		

	public static void ClassToFile(object obj, string filename, SavingPath pathType = SavingPath.PersistentDataPath,
		FileExtension extensionType = FileExtension.json){
		string fullPath = PathReturner.ReturnSavePath(pathType) + "/" 
			+ filename + PathReturner.ReturnFileExtension(extensionType);
		File.WriteAllText(fullPath, JsonUtility.ToJson(obj));
	}

	static class PathReturner{
		public static string ReturnSavePath(SavingPath pathType){
			switch(pathType){
			case SavingPath.PersistentDataPath:
				return Application.persistentDataPath;
			case SavingPath.TemporaryCachePath:
				return Application.temporaryCachePath;
			}
			Debug.LogWarning("Error on SavingPathType");
			return "";
		}

		public static string ReturnLoadPath(LoadingPath pathType){
			switch(pathType){
			case LoadingPath.PersistentDataPath:
				return Application.persistentDataPath;
			case LoadingPath.TemporaryCachePath:
				return Application.temporaryCachePath;
			case LoadingPath.DataPath:
				return Application.dataPath;
			}
			Debug.LogWarning("Error on SavingPathType");
			return "";
		}

		public static string ReturnFileExtension(FileExtension extensionType){
			switch(extensionType){
			case FileExtension.json:
				return ".json";
			case FileExtension.txt:
				return ".txt";
			default:
				return "";
			}
		}
	}
}
	
public enum SavingPath{
	PersistentDataPath,
	TemporaryCachePath, 
}

public enum LoadingPath{
	PersistentDataPath,
	TemporaryCachePath,
	DataPath
}

public enum FileExtension{
	txt,
	json,
	none
}
