Linked applications Bitbucket
Teams
Projects
Repositories
Snippets
owner/repository
Find a repository…
 Logged in as eestradalucero
 
Emilio Estrada  JsonIO
Commits

 Emilio Estrada  committed dc19345
5 days ago
Approve
README.md edited online with Bitbucket
Participants
 eestradalucero
Parent commitsad3a88a
Raw commitView raw commit
Watch Stop watching
Comments (0)


What do you want to say?
Files changed (1)
+37 -0 README.md
File README.md
Side-by-side diff  View file   Comment   More
+# README #
+
+
+## What is this repository for? ##
+**JsonIO is a wrapper class for Unity3D's JsonUtilities** that lets you serialize and deserialize Monobehaviour, ScriptableObjects or System.Serializable classes directly into a file (*.json, *txt).
+
+
+
+## How do I get set up? ##
+
+### Importing to a project ###
+Just import the class or folder from this repository and you are ready to go. JsonIO is a public static class anyways. ;)
+
+### Usage ###
+
+*Note: For the moment SavingPath and LoadingPath are reversed, it will be fixed in the next commit*
+
+There are really 3 core functions to JsonIO:
+
+**ClassToFile:** Saves a Class into a Json string, which is later saved at the specified directory (Defaults to Application.PersistentDataPath) with an specified extension (Defaults to .json); it has no return type.
+
+
+> Example:
+JsonIO.ClassToFile(foo, LoadingPath.PersistentDataPath, FileExtension.json); //Last two parameters are optional.
+
+**FileToClass:** Reads a file with an specified name, at an specified directory (Defaults to Application.PersistentDataPath) and an specified extension (Defaults to .json) and returns an object of class T.
Add a comment to this line
+
+> Example:
+Foo foo = FileToClass<Foo>("foo", SavingPath.PersistentDataPath, FileExtension.json); //Last two parameters are optional
+
+**FileToMonobehaviour:** Reads a file with an specified name, at an specified directory (Defaults to Application.PersistentDataPath) and an specified extension (Defaults to .json) and overwrites the object in the argument to be filled with the information in the structured JSON.
Add a comment to this line
+
+> Example:
+FileToMonobehaviour(monobehaviourScript, filename, SavingPath.PersistentDataPath, FileExtension.json); //Last two parameters are optional
+
+**FileToMonobehaviour:** Works exactly the same as FileToMonobehaviour but for scriptableObjects.
+
+FileToString: Works exactly the same as FileToClass, but it returns a string.
+
+
+
+### Who do I talk to? ###
+
+* My name is Emilio Estrada; you can contact me at egestradalucero@gmail.com, yomilo@gmail.com, or at twitter at: https://twitter.com/Milo_del_mal
Blog  Support  Plans & pricing  Documentation  API  Site status  Version info  Terms of service  Privacy policy
JIRA  Confluence  Bamboo  SourceTree  HipChat
Atlassian